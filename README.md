# DetailsDataBase

JavaFX + MySQL

DataBase configuration in `\src\main\resources\application.properties`.

By default use MySql database.

Default database schema located in `\src\main\resources\schema.sql`. (Only for MySql)

#### How to run:

 - Open project in IntelijIDEA
 - Setup Project SDK if not selected it. `Required Java 8`.
 - Find `Main` class in `src` folder. Open this class and press green arrow.
 - For compile into `.jar` file, use maven lifecycle `package`.
