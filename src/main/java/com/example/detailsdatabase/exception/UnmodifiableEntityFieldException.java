package com.example.detailsdatabase.exception;

public class UnmodifiableEntityFieldException extends RuntimeException {
    public UnmodifiableEntityFieldException() {
        super();
    }

    public UnmodifiableEntityFieldException(String message) {
        super(message);
    }

    public UnmodifiableEntityFieldException(String message, Throwable cause) {
        super(message, cause);
    }

    public UnmodifiableEntityFieldException(Throwable cause) {
        super(cause);
    }

    protected UnmodifiableEntityFieldException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
