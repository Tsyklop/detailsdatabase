package com.example.detailsdatabase.controller;

import com.example.detailsdatabase.exception.DBException;
import com.example.detailsdatabase.pesistence.entity.UserEntity;
import com.example.detailsdatabase.pesistence.repository.impl.UserRepositoryImpl;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;

public class SignInController extends AbstractController {

    @FXML
    private TextField loginInput;

    @FXML
    private PasswordField passwordInput;

    private UserRepositoryImpl userRepository;

    private static final Logger LOGGER = LoggerFactory.getLogger(SignInController.class);

    public void initialize(URL location, ResourceBundle resources) {
        this.userRepository = new UserRepositoryImpl();
    }

    public void onSignIn(MouseEvent mouseEvent) {

        Button button = (Button) mouseEvent.getSource();


        if (validateData()) {

            try {

                button.setDisable(true);

                Optional<UserEntity> userEntity = userRepository.findByLogin(this.loginInput.getText());

                if (!userEntity.isPresent() || !userEntity.get().getPassword().equals(this.passwordInput.getText())) {
                    window.error("Лоигн и (или) пароль неверны");
                    return;
                }

                window.show("window");

            } catch (DBException e) {
                LOGGER.error("SIGN IN ERROR", e);
                window.error("Ошибка входа: " + e.getMessage());
            } finally {
                button.setDisable(false);
            }

        }

    }

    public void openSignUpPage(MouseEvent mouseEvent) {
        window.show("signUp");
    }

    protected boolean validateData() {

        if (StringUtils.isBlank(this.loginInput.getText())) {
            window.error("Введите Логин");
            return false;
        }

        if (StringUtils.isBlank(this.passwordInput.getText())) {
            window.error("Введите Пароль");
            return false;
        }

        return true;

    }

}
