package com.example.detailsdatabase.controller.order;

import com.example.detailsdatabase.controller.ModalController;
import com.example.detailsdatabase.model.Order;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;

import java.net.URL;
import java.util.ResourceBundle;

public class OrderSuccessController extends ModalController {

    @FXML
    private Label price;

    @Override
    public void initialize(URL location, ResourceBundle resources) { }

    @Override
    public void modalInitialized() {

        if (this.data != null && this.data.containsKey("order")) {

            Order order = (Order) this.data.get("order");

            this.price.setText(order.getPrice() + " грн.");

        }

    }

    public void onClose(MouseEvent mouseEvent) {
        closeModal();
    }
}
