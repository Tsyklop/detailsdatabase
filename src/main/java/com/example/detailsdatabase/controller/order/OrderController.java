package com.example.detailsdatabase.controller.order;

import com.example.detailsdatabase.controller.ModalController;
import com.example.detailsdatabase.exception.DBException;
import com.example.detailsdatabase.model.Order;
import com.example.detailsdatabase.pesistence.entity.DetailEntity;
import com.example.detailsdatabase.pesistence.entity.MaterialEntity;
import com.example.detailsdatabase.pesistence.repository.impl.DetailRepositoryImpl;
import com.example.detailsdatabase.pesistence.repository.impl.MaterialRepositoryImpl;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.util.Callback;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URL;
import java.util.*;
import java.util.stream.Collectors;

public class OrderController extends ModalController {

    @FXML
    private Button orderRemoveButton;

    @FXML
    private TextField countInput;

    @FXML
    private ComboBox<DetailEntity> detailComboBox;

    @FXML
    private ListView<OrderItem> orderListListView;

    private DetailRepositoryImpl detailRepository;
    private MaterialRepositoryImpl materialRepository;

    private ObservableList<DetailEntity> detailEntities;

    private static final Logger LOGGER = LoggerFactory.getLogger(OrderController.class);

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        this.detailRepository = new DetailRepositoryImpl();
        this.materialRepository = new MaterialRepositoryImpl();

        this.orderRemoveButton.setDisable(true);

        this.detailComboBox.setButtonCell(new ListCell<DetailEntity>() {
            @Override
            protected void updateItem(DetailEntity item, boolean empty) {
                super.updateItem(item, empty);
                if (item == null || empty) {
                    setGraphic(null);
                } else {
                    setText(item.getName());
                }
            }
        });

        this.detailComboBox.setCellFactory(new Callback<ListView<DetailEntity>, ListCell<DetailEntity>>() {
            @Override
            public ListCell<DetailEntity> call(ListView<DetailEntity> param) {
                return new ListCell<DetailEntity>() {
                    @Override
                    protected void updateItem(DetailEntity item, boolean empty) {
                        super.updateItem(item, empty);
                        if (item == null || empty) {
                            setGraphic(null);
                        } else {
                            setText(item.getName());
                        }
                    }
                };
            }
        });

        this.orderListListView.setCellFactory(new Callback<ListView<OrderItem>, ListCell<OrderItem>>() {
            @Override
            public ListCell<OrderItem> call(ListView<OrderItem> param) {
                return new ListCell<OrderItem>() {
                    @Override
                    protected void updateItem(OrderItem item, boolean empty) {
                        super.updateItem(item, empty);
                        if (item == null || empty) {
                            setText("");
                            setGraphic(null);
                        } else {
                            setText("Деталь - " + item.getDetailEntity().getName() + " \t\tКоличество: " + item.getCount());
                        }
                    }
                };
            }
        });

        this.orderListListView.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> orderRemoveButton.setDisable(newValue == null));

        this.detailEntities = FXCollections.observableList(this.detailRepository.getAll());

        this.detailComboBox.getItems().setAll(this.detailEntities);

    }

    public void onRemoveOrderItem(MouseEvent mouseEvent) {

        Button button = (Button) mouseEvent.getSource();

        button.setDisable(true);

        try {

            if (!this.orderListListView.getSelectionModel().isEmpty()) {

                OrderItem orderItem = this.orderListListView.getSelectionModel().getSelectedItem();

                this.orderListListView.getItems().removeIf(oi -> oi.equals(orderItem));

                reloadDetailsComboBox();

            }

        } finally {
            button.setDisable(false);
        }

    }

    public void onDetailAddToOrder(MouseEvent mouseEvent) {

        Button button = (Button) mouseEvent.getSource();

        if (validateData()) {

            button.setDisable(true);

            try {

                DetailEntity detailEntity = this.detailComboBox.getValue();

                this.orderListListView.getItems().add(new OrderItem(Integer.valueOf(this.countInput.getText()), detailEntity));

                this.detailComboBox.getSelectionModel().clearSelection();

                reloadDetailsComboBox();

                this.countInput.setText("");

            } finally {
                button.setDisable(false);
            }

        }

    }

    public void onOrderSubmit(MouseEvent mouseEvent) {

        Button button = (Button) mouseEvent.getSource();

        try {

            button.setDisable(true);

            if (this.orderListListView.getItems().isEmpty()) {
                window.error("Выберите деталь(и)");
                return;
            }

            double price = 0.0;

            Map<Long, Integer> bookedMaterials = new HashMap<>();

            for (OrderItem orderItem : this.orderListListView.getItems()) {

                DetailEntity detailEntity = orderItem.getDetailEntity();

                int materialsCount = detailEntity.getMaterialCount() * orderItem.getCount();

                Optional<MaterialEntity> materialEntityOptional = materialRepository.getById(detailEntity.getMaterialId());

                if (!materialEntityOptional.isPresent()) {
                    window.error("Материал для тедали " + detailEntity.getName() + " не найден");
                    return;
                }

                MaterialEntity materialEntity = materialEntityOptional.get();

                if (bookedMaterials.containsKey(materialEntity.getId())) {
                    bookedMaterials.put(materialEntity.getId(), bookedMaterials.get(materialEntity.getId()) + (materialsCount));
                } else {
                    bookedMaterials.put(materialEntity.getId(), materialsCount);
                }

                if (bookedMaterials.get(materialEntity.getId()) > materialEntity.getCount()) {
                    window.error("Материала '" + materialEntity.getName() + "' не хватает. Необходимо: " + materialsCount + ". На Складе: " + materialEntity.getCount());
                    return;
                }

                price += (detailEntity.getWorkPrice() * orderItem.getCount()) + (materialEntity.getPrice() * materialsCount);

            }

            window.showModal("modal/orderSuccess", true, Collections.singletonMap("order", new Order(price, this.orderListListView.getItems().stream().map(OrderItem::getDetailEntity).collect(Collectors.toList()))));

            closeModal();

        } catch (DBException e) {
            LOGGER.error("ORDER CREATE ERROR:", e);
            window.error("Ошибка: " + e.getMessage());
        } finally {
            button.setDisable(false);
        }

    }

    @Override
    protected boolean validateData() {

        if (this.detailComboBox.getSelectionModel().isEmpty()) {
            window.error("Выберите деталь");
            return false;
        }

        if (StringUtils.isBlank(this.countInput.getText())) {
            window.error("Введите количество");
            return false;
        }

        try {
            Integer.valueOf(this.countInput.getText());
        } catch (NumberFormatException e) {
            window.error("Количество деталей введено некорректно");
            return false;
        }

        return true;
    }

    @Override
    protected void resetFieldsData() {

        this.countInput.setText("");
        this.detailComboBox.getSelectionModel().clearSelection();

    }

    private void reloadDetailsComboBox() {
        this.detailComboBox.getItems().setAll(detailEntities.stream().filter(de -> this.orderListListView.getItems().stream().noneMatch(orderItem -> orderItem.getDetailEntity().getId().equals(de.getId()))).collect(Collectors.toList()));
    }

    private static class OrderItem {

        private final Integer count;

        private final DetailEntity detailEntity;

        public OrderItem(Integer count, DetailEntity detailEntity) {
            this.count = count;
            this.detailEntity = detailEntity;
        }

        public Integer getCount() {
            return count;
        }

        public DetailEntity getDetailEntity() {
            return detailEntity;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            OrderItem orderItem = (OrderItem) o;
            return count.equals(orderItem.count) &&
                    detailEntity.equals(orderItem.detailEntity);
        }

        @Override
        public int hashCode() {
            return Objects.hash(count, detailEntity);
        }

    }

}
