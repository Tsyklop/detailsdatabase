package com.example.detailsdatabase.controller;

import com.example.detailsdatabase.exception.DBException;
import com.example.detailsdatabase.model.Gender;
import com.example.detailsdatabase.pesistence.entity.UserEntity;
import com.example.detailsdatabase.pesistence.repository.impl.UserRepositoryImpl;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URL;
import java.util.ResourceBundle;

public final class SignUpController extends AbstractController {

    @FXML
    private TextField firstNameInput;

    @FXML
    private TextField lastNameInput;

    @FXML
    private TextField phoneInput;

    @FXML
    private TextField loginInput;

    @FXML
    private PasswordField passwordInput;

    @FXML
    private RadioButton genderMale;

    @FXML
    private RadioButton genderFemale;

    private UserRepositoryImpl userRepository;

    private static final Logger LOGGER = LoggerFactory.getLogger(SignUpController.class);

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        userRepository = new UserRepositoryImpl();

        ToggleGroup group = new ToggleGroup();

        genderMale.setSelected(true);
        genderMale.setToggleGroup(group);

        genderFemale.setSelected(false);
        genderFemale.setToggleGroup(group);

    }

    public void onSignUp(MouseEvent mouseEvent) {

        Button button = (Button) mouseEvent.getSource();

        if (validateData()) {

            try {

                button.setDisable(true);

                if (userRepository.existsByLogin(this.loginInput.getText())) {
                    window.error("Пользователь с таким логином уже существует.");
                    return;
                }

                UserEntity userEntity = new UserEntity();

                userEntity.setFirstName(this.firstNameInput.getText());
                userEntity.setLastName(this.lastNameInput.getText());
                userEntity.setPhone(this.phoneInput.getText());

                if (genderMale.isSelected()) {
                    userEntity.setGender(Gender.MALE);
                } else if (genderFemale.isSelected()) {
                    userEntity.setGender(Gender.FEMALE);
                }

                userEntity.setLogin(this.loginInput.getText());
                userEntity.setPassword(this.passwordInput.getText());

                userRepository.insert(userEntity);

                resetFieldsData();

                window.success("Вы успешно зарегистрировались!");

                openSignInPage(mouseEvent);

            } catch (DBException e) {
                LOGGER.error("SIGNUP ERROR:", e);
                window.error("Ошибка: " + e.getMessage());
            } finally {
                button.setDisable(false);
            }

        }

    }

    public void openSignInPage(MouseEvent mouseEvent) {
        window.show("signIn");
    }

    @Override
    protected boolean validateData() {

        if (StringUtils.isBlank(this.firstNameInput.getText())) {
            window.error("Введите Имя");
            return false;
        }

        if (StringUtils.isBlank(this.lastNameInput.getText())) {
            window.error("Введите Фамилию");
            return false;
        }

        if (StringUtils.isBlank(this.phoneInput.getText())) {
            window.error("Введите Телефон");
            return false;
        }

        if (StringUtils.isBlank(this.loginInput.getText())) {
            window.error("Введите Логин");
            return false;
        }

        if (StringUtils.isBlank(this.passwordInput.getText())) {
            window.error("Введите Пароль");
            return false;
        }

        return true;

    }

    @Override
    protected void resetFieldsData() {

        this.firstNameInput.setText("");
        this.lastNameInput.setText("");
        this.phoneInput.setText("");
        this.loginInput.setText("");
        this.passwordInput.setText("");

    }

}
