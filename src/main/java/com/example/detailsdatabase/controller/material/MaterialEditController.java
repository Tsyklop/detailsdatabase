package com.example.detailsdatabase.controller.material;

import com.example.detailsdatabase.exception.DBException;
import com.example.detailsdatabase.pesistence.entity.MaterialEntity;
import com.example.detailsdatabase.pesistence.repository.impl.MaterialRepositoryImpl;
import javafx.scene.control.Button;
import javafx.scene.input.MouseEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URL;
import java.util.ResourceBundle;

public class MaterialEditController extends BaseMaterialController {

    private static final Logger LOGGER = LoggerFactory.getLogger(MaterialEditController.class);

    @Override
    public void modalInitialized() {

        MaterialEntity materialEntity = getMaterialEntity();

        if (materialEntity != null) {

            this.nameInput.setText(materialEntity.getName());
            this.unitInput.setText(materialEntity.getUnit());
            this.articleInput.setText(materialEntity.getArticle());
            this.priceInput.setText(materialEntity.getPrice().toString());
            this.shopInput.setText(materialEntity.getShop());
            this.countInput.setText(materialEntity.getCount().toString());

        } else {
            window.error("Материал не найден");
            closeModal();
        }

    }

    public void onSave(MouseEvent mouseEvent) {

        Button button = (Button) mouseEvent.getSource();

        if (validateData()) {

            try {

                button.setDisable(true);

                MaterialEntity materialEntity = getMaterialEntity();

                if (materialEntity != null) {

                    if (!materialEntity.getName().equals(this.nameInput.getText()) && materialRepository.existsByName(this.nameInput.getText())) {
                        window.error("Материал с таким названием уже существует");
                        return;
                    }

                    fillMaterialEntity(materialEntity);

                    materialRepository.update(materialEntity);

                    window.success("Материал успешно обновлен!");

                    closeModal();

                } else {
                    window.error("Материал не найден");
                    closeModal();
                }

            } catch (DBException e) {
                LOGGER.error("MATERIAL CREATE ERROR:", e);
                window.error("Ошибка: " + e.getMessage());
            } finally {
                button.setDisable(false);
            }

        }

    }

    private MaterialEntity getMaterialEntity() {
        if (this.data != null && this.data.containsKey("material")) {
            return (MaterialEntity) this.data.get("material");
        }
        return null;
    }

}
