package com.example.detailsdatabase.controller.material;

import com.example.detailsdatabase.exception.DBException;
import com.example.detailsdatabase.pesistence.entity.MaterialEntity;
import com.example.detailsdatabase.pesistence.repository.impl.MaterialRepositoryImpl;
import javafx.scene.control.Button;
import javafx.scene.input.MouseEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URL;
import java.util.ResourceBundle;

public class MaterialCreateController extends BaseMaterialController {

    private static final Logger LOGGER = LoggerFactory.getLogger(MaterialCreateController.class);

    public void onSave(MouseEvent mouseEvent) {

        Button button = (Button) mouseEvent.getSource();

        if(validateData()) {

            try {

                button.setDisable(true);

                if (materialRepository.existsByName(this.nameInput.getText())) {
                    window.error("Материал с таким названием уже существует");
                    return;
                }

                MaterialEntity materialEntity = new MaterialEntity();

                fillMaterialEntity(materialEntity);

                materialRepository.insert(materialEntity);

                resetFieldsData();

                window.success("Материал успешно добавлен!");

                closeModal();

            } catch (DBException e) {
                LOGGER.error("MATERIAL CREATE ERROR:", e);
                window.error("Ошибка: " + e.getMessage());
            } finally {
                button.setDisable(false);
            }

        }

    }

}
