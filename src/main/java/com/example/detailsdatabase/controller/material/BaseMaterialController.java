package com.example.detailsdatabase.controller.material;

import com.example.detailsdatabase.controller.ModalController;
import com.example.detailsdatabase.pesistence.entity.MaterialEntity;
import com.example.detailsdatabase.pesistence.repository.impl.MaterialRepositoryImpl;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import org.apache.commons.lang3.StringUtils;

import java.net.URL;
import java.util.ResourceBundle;

public abstract class BaseMaterialController extends ModalController {

    @FXML
    protected TextField nameInput;

    @FXML
    protected TextField unitInput;

    @FXML
    protected TextField articleInput;

    @FXML
    protected TextField shopInput;

    @FXML
    protected TextField priceInput;

    @FXML
    protected TextField countInput;

    protected MaterialRepositoryImpl materialRepository;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        this.materialRepository = new MaterialRepositoryImpl();
    }

    public abstract void onSave(MouseEvent mouseEvent);

    protected void fillMaterialEntity(MaterialEntity materialEntity) {
        materialEntity.setName(this.nameInput.getText());
        materialEntity.setUnit(this.unitInput.getText());
        materialEntity.setArticle(this.articleInput.getText());
        materialEntity.setPrice(Double.valueOf(this.priceInput.getText()));
        materialEntity.setShop(this.shopInput.getText());
        materialEntity.setCount(Integer.valueOf(this.countInput.getText()));
    }

    @Override
    protected boolean validateData() {

        if(StringUtils.isBlank(this.nameInput.getText())) {
            window.error("Enter Name");
            return false;
        }

        if(StringUtils.isBlank(this.unitInput.getText())) {
            window.error("Enter Unit");
            return false;
        }

        if(StringUtils.isBlank(this.articleInput.getText())) {
            window.error("Enter Article");
            return false;
        }

        if(StringUtils.isBlank(this.shopInput.getText())) {
            window.error("Enter Shop Name");
            return false;
        }

        if(StringUtils.isBlank(this.priceInput.getText())) {
            window.error("Enter Price");
            return false;
        }

        try {
            Double.valueOf(this.priceInput.getText());
        } catch (NumberFormatException e) {
            window.error("Price Is Incorrect");
            return false;
        }

        if(StringUtils.isBlank(this.countInput.getText())) {
            window.error("Enter Count");
            return false;
        }

        try {
            Integer.valueOf(this.countInput.getText());
        } catch (NumberFormatException e) {
            window.error("Count Is Incorrect");
            return false;
        }

        return true;
    }

    @Override
    protected void resetFieldsData() {

        this.nameInput.setText("");
        this.unitInput.setText("");
        this.articleInput.setText("");
        this.priceInput.setText("");
        this.shopInput.setText("");
        this.countInput.setText("");

    }

}
