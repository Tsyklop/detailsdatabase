package com.example.detailsdatabase.controller;

import com.example.detailsdatabase.window.Window;
import javafx.fxml.Initializable;

public abstract class AbstractController implements Initializable {

    protected Window window = Window.getInstance();

    protected boolean validateData() { return false; };
    protected void resetFieldsData() {};

}
