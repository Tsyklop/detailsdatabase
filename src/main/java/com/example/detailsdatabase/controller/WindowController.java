package com.example.detailsdatabase.controller;

import com.example.detailsdatabase.exception.DBException;
import com.example.detailsdatabase.pesistence.entity.DetailEntity;
import com.example.detailsdatabase.pesistence.entity.MaterialEntity;
import com.example.detailsdatabase.pesistence.repository.impl.DetailRepositoryImpl;
import com.example.detailsdatabase.pesistence.repository.impl.MaterialRepositoryImpl;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.util.Callback;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URL;
import java.util.*;

import static com.example.detailsdatabase.util.Util.exit;

public class WindowController extends AbstractController {

    @FXML
    private TableView<DetailEntity> detailsTable;

    @FXML
    private TableView<MaterialEntity> materialsTable;

    @FXML
    private Label detailsCount;

    @FXML
    private Label materialsCount;

    @FXML
    private TextField detailsSearchInput;

    @FXML
    private TextField materialsSearchInput;

    @FXML
    private TableColumn<DetailEntity, Long> detailColumnId;

    @FXML
    private TableColumn<DetailEntity, String> detailColumnName;

    @FXML
    private TableColumn<DetailEntity, String> detailColumnUnit;

    @FXML
    private TableColumn<DetailEntity, String> detailColumnArticle;

    @FXML
    private TableColumn<DetailEntity, Double> detailColumnWorkPrice;

    @FXML
    private TableColumn<DetailEntity, String> detailColumnMaterial;

    @FXML
    private TableColumn<DetailEntity, Integer> detailColumnMaterialCount;

    @FXML
    private TableColumn<MaterialEntity, Long> materialColumnId;

    @FXML
    private TableColumn<MaterialEntity, String> materialColumnName;

    @FXML
    private TableColumn<MaterialEntity, String> materialColumnUnit;

    @FXML
    private TableColumn<MaterialEntity, String> materialColumnArticle;

    @FXML
    private TableColumn<MaterialEntity, Double> materialColumnPrice;

    @FXML
    private TableColumn<MaterialEntity, String> materialColumnShop;

    @FXML
    private TableColumn<MaterialEntity, Integer> materialColumnCount;

    private DetailRepositoryImpl detailRepository = new DetailRepositoryImpl();
    private MaterialRepositoryImpl materialRepository = new MaterialRepositoryImpl();

    private static final Logger LOGGER = LoggerFactory.getLogger(WindowController.class);

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        LOGGER.info("WINDOW INITIALIZED");

        detailColumnId.setCellValueFactory(new PropertyValueFactory<>("id"));
        detailColumnName.setCellValueFactory(new PropertyValueFactory<>("name"));
        detailColumnUnit.setCellValueFactory(new PropertyValueFactory<>("unit"));
        detailColumnArticle.setCellValueFactory(new PropertyValueFactory<>("article"));
        detailColumnWorkPrice.setCellValueFactory(new PropertyValueFactory<>("workPrice"));

        detailColumnMaterial.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<DetailEntity, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<DetailEntity, String> param) {

                try {

                    Optional<MaterialEntity> materialEntity = materialRepository.getById(param.getValue().getMaterialId());

                    if (materialEntity.isPresent()) {
                        return new SimpleObjectProperty<>(materialEntity.get().getName());
                    }

                } catch (DBException e) {
                    LOGGER.error("ERROR WHEN RETREIVING MATERIAL", e);
                }

                return new SimpleObjectProperty<>(" - ");

            }
        });

        detailColumnMaterialCount.setCellValueFactory(new PropertyValueFactory<>("materialCount"));

        materialColumnId.setCellValueFactory(new PropertyValueFactory<>("id"));
        materialColumnName.setCellValueFactory(new PropertyValueFactory<>("name"));
        materialColumnUnit.setCellValueFactory(new PropertyValueFactory<>("unit"));
        materialColumnArticle.setCellValueFactory(new PropertyValueFactory<>("article"));
        materialColumnPrice.setCellValueFactory(new PropertyValueFactory<>("price"));
        materialColumnShop.setCellValueFactory(new PropertyValueFactory<>("shop"));
        materialColumnCount.setCellValueFactory(new PropertyValueFactory<>("count"));

        detailsTable.setRowFactory(tv -> {
            TableRow<DetailEntity> row = new TableRow<>();
            row.setOnMouseClicked(event -> {
                if (event.getClickCount() == 2 && (!row.isEmpty())) {
                    window.showModal("modal/detailEdit", true, Collections.singletonMap("detail", row.getItem()));
                    loadDetails();
                }
            });
            return row;
        });

        materialsTable.setRowFactory(tv -> {
            TableRow<MaterialEntity> row = new TableRow<>();
            row.setOnMouseClicked(event -> {
                if (event.getClickCount() == 2 && (!row.isEmpty())) {
                    window.showModal("modal/materialEdit", true, Collections.singletonMap("material", row.getItem()));
                    loadMaterials();
                }
            });
            return row;
        });

    }

    public void onExit(ActionEvent actionEvent) {
        window.show("signIn");
    }

    public void onShutdown(ActionEvent actionEvent) {
        exit();
    }

    public void onAbout(ActionEvent actionEvent) {
        window.showModal("modal/about");
    }

    public void onOrder(MouseEvent mouseEvent) {
        window.showModal("modal/order");
    }

    public void onDetailCreate(MouseEvent mouseEvent) {
        window.showModal("modal/detailCreate", true);
        loadDetails();
    }

    public void onSearchDetail(MouseEvent mouseEvent) {

    }

    public void onMaterialCreate(MouseEvent mouseEvent) {
        window.showModal("modal/materialCreate", true);
        loadMaterials();
    }

    public void onMaterialsSearch(MouseEvent mouseEvent) {

    }

    public void onMaterialsTabSelectionChanged(Event event) {

        Tab materialsTab = (Tab) event.getSource();

        if (materialsTab.isSelected()) {
            LOGGER.info("load materials");
            loadMaterials();
            materialsCount.setText(String.valueOf(materialsTable.getItems().size()));
        } else {
            LOGGER.info("unload materials");
            materialsTable.getItems().setAll();
        }

    }

    public void onDetailsTabSelectionChanged(Event event) {

        Tab detailsTab = (Tab) event.getSource();

        if (detailsTab.isSelected()) {
            LOGGER.info("load details");

            loadDetails();
            detailsCount.setText(String.valueOf(detailsTable.getItems().size()));

        } else {
            LOGGER.info("unload details");
            detailsTable.getItems().setAll();
        }

    }

    private void loadDetails() {
        try {
            detailsTable.getItems().setAll(FXCollections.observableList(this.detailRepository.getAll()));
        } catch (DBException e) {
            window.error("Ошибка при загрузке деталей: " + e.getMessage());
        }
    }

    private void loadMaterials() {
        try {
            materialsTable.getItems().setAll(FXCollections.observableList(this.materialRepository.getAll()));
        } catch (DBException e) {
            window.error("Ошибка при загрузке материалов: " + e.getMessage());
        }
    }

}
