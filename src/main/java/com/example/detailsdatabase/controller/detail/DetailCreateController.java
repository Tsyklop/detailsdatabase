package com.example.detailsdatabase.controller.detail;

import com.example.detailsdatabase.exception.DBException;
import com.example.detailsdatabase.pesistence.entity.DetailEntity;
import com.example.detailsdatabase.pesistence.entity.MaterialEntity;
import com.example.detailsdatabase.pesistence.repository.impl.DetailRepositoryImpl;
import com.example.detailsdatabase.pesistence.repository.impl.MaterialRepositoryImpl;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.util.Callback;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URL;
import java.util.ResourceBundle;

public class DetailCreateController extends BaseDetailController {

    private static final Logger LOGGER = LoggerFactory.getLogger(DetailCreateController.class);

    public void onSave(MouseEvent mouseEvent) {

        Button button = (Button) mouseEvent.getSource();

        if (validateData()) {

            try {

                button.setDisable(true);

                if (detailRepository.existsByName(this.nameInput.getText())) {
                    window.error("Деталь с таким именем уже существует");
                    return;
                }

                DetailEntity detailEntity = new DetailEntity();

                fillDetailEntity(detailEntity);

                detailRepository.insert(detailEntity);

                window.success("Деталь успешно добавлена!");

                resetFieldsData();

                closeModal();

            } catch (DBException e) {
                LOGGER.error("DETAIL CREATE ERROR:", e);
                window.error("Ошибка: " + e.getMessage());
            } finally {
                button.setDisable(false);
            }

        }

    }

}
