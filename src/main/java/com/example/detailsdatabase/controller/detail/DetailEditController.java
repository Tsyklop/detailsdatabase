package com.example.detailsdatabase.controller.detail;

import com.example.detailsdatabase.exception.DBException;
import com.example.detailsdatabase.pesistence.entity.DetailEntity;
import com.example.detailsdatabase.pesistence.entity.MaterialEntity;
import com.example.detailsdatabase.pesistence.repository.impl.DetailRepositoryImpl;
import com.example.detailsdatabase.pesistence.repository.impl.MaterialRepositoryImpl;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.util.Callback;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;

public class DetailEditController extends BaseDetailController {

    private static final Logger LOGGER = LoggerFactory.getLogger(DetailEditController.class);

    @Override
    public void modalInitialized() {

        DetailEntity detailEntity = getDetailEntity();

        if (detailEntity != null) {

            this.nameInput.setText(detailEntity.getName());
            this.unitInput.setText(detailEntity.getUnit());
            this.articleInput.setText(detailEntity.getArticle());
            this.workPriceInput.setText(detailEntity.getWorkPrice().toString());
            this.materialCountInput.setText(detailEntity.getMaterialCount().toString());

            Optional<MaterialEntity> materialEntity = this.materialRepository.getById(detailEntity.getMaterialId());

            materialEntity.ifPresent(entity -> this.materialComboBox.getSelectionModel().clearAndSelect(this.materialComboBox.getItems().indexOf(entity)));

        } else {
            window.error("Деталь не найдена");
            closeModal();
        }

    }

    public void onSave(MouseEvent mouseEvent) {

        Button button = (Button) mouseEvent.getSource();

        if (validateData()) {

            try {

                button.setDisable(true);

                DetailEntity detailEntity = getDetailEntity();

                if (detailEntity != null) {

                    if (!detailEntity.getName().equals(this.nameInput.getText()) && detailRepository.existsByName(this.nameInput.getText())) {
                        window.error("Деталь с таким именем уже существует");
                        return;
                    }

                    fillDetailEntity(detailEntity);

                    detailRepository.update(detailEntity);

                    window.success("Деталь успешно обновлена!");

                    closeModal();

                } else {
                    window.error("Деталь не найдена");
                    closeModal();
                }

            } catch (DBException e) {
                LOGGER.error("DETAIL CREATE ERROR:", e);
                window.error("Ошибка: " + e.getMessage());
            } finally {
                button.setDisable(false);
            }

        }

    }

    private DetailEntity getDetailEntity() {
        if (this.data != null && this.data.containsKey("detail")) {
            return (DetailEntity) this.data.get("detail");
        }
        return null;
    }

}
