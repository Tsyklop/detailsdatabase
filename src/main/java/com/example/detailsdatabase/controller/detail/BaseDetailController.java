package com.example.detailsdatabase.controller.detail;

import com.example.detailsdatabase.controller.ModalController;
import com.example.detailsdatabase.pesistence.entity.DetailEntity;
import com.example.detailsdatabase.pesistence.entity.MaterialEntity;
import com.example.detailsdatabase.pesistence.repository.impl.DetailRepositoryImpl;
import com.example.detailsdatabase.pesistence.repository.impl.MaterialRepositoryImpl;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.util.Callback;
import org.apache.commons.lang3.StringUtils;

import java.net.URL;
import java.util.ResourceBundle;

public abstract class BaseDetailController extends ModalController {

    @FXML
    protected TextField nameInput;

    @FXML
    protected TextField unitInput;

    @FXML
    protected TextField articleInput;

    @FXML
    protected TextField workPriceInput;

    @FXML
    protected TextField materialCountInput;

    @FXML
    protected ComboBox<MaterialEntity> materialComboBox;

    protected DetailRepositoryImpl detailRepository;

    protected MaterialRepositoryImpl materialRepository;

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        this.detailRepository = new DetailRepositoryImpl();
        this.materialRepository = new MaterialRepositoryImpl();

        this.materialComboBox.setCellFactory(new Callback<ListView<MaterialEntity>, ListCell<MaterialEntity>>() {
            @Override
            public ListCell<MaterialEntity> call(ListView<MaterialEntity> param) {
                return new ListCell<MaterialEntity>() {
                    @Override
                    protected void updateItem(MaterialEntity item, boolean empty) {
                        super.updateItem(item, empty);
                        if (item == null || empty) {
                            setGraphic(null);
                        } else {
                            setText(item.getName());
                        }
                    }
                };
            }
        });

        this.materialComboBox.setButtonCell(new ListCell<MaterialEntity>() {
            @Override
            protected void updateItem(MaterialEntity item, boolean empty) {
                super.updateItem(item, empty);
                if (item == null || empty) {
                    setGraphic(null);
                } else {
                    setText(item.getName());
                }
            }
        });

        this.materialComboBox.getItems().setAll(FXCollections.observableList(this.materialRepository.getAll()));

    }

    protected void fillDetailEntity(DetailEntity detailEntity) {
        detailEntity.setName(this.nameInput.getText());
        detailEntity.setUnit(this.unitInput.getText());
        detailEntity.setArticle(this.articleInput.getText());
        detailEntity.setWorkPrice(Double.valueOf(this.workPriceInput.getText()));
        detailEntity.setMaterialId(this.materialComboBox.getSelectionModel().getSelectedItem().getId());
        detailEntity.setMaterialCount(Integer.valueOf(this.materialCountInput.getText()));
    }

    protected boolean validateData() {

        if (StringUtils.isBlank(this.nameInput.getText())) {
            window.error("Введите Имя");
            return false;
        }

        if (StringUtils.isBlank(this.unitInput.getText())) {
            window.error("Введите Иденицу Измерения");
            return false;
        }

        if (StringUtils.isBlank(this.articleInput.getText())) {
            window.error("Введите Артикул");
            return false;
        }

        if (StringUtils.isBlank(this.workPriceInput.getText())) {
            window.error("Введите стоимость работы");
            return false;
        }

        try {
            Double.valueOf(this.workPriceInput.getText());
        } catch (NumberFormatException e) {
            window.error("Значение цены работы некорректное");
            return false;
        }

        if (this.materialComboBox.getSelectionModel().isEmpty()) {
            window.error("Выберите материал");
            return false;
        }

        if (StringUtils.isBlank(this.materialCountInput.getText())) {
            window.error("Введите кол-во материала");
            return false;
        }

        try {
            Integer.valueOf(this.materialCountInput.getText());
        } catch (NumberFormatException e) {
            window.error("Значение кол-ва материала некорректное");
            return false;
        }

        return true;
    }

    @Override
    protected void resetFieldsData() {

        this.nameInput.setText("");
        this.unitInput.setText("");
        this.articleInput.setText("");
        this.workPriceInput.setText("");
        this.materialCountInput.setText("");

        this.materialComboBox.getSelectionModel().clearSelection();

    }

}
