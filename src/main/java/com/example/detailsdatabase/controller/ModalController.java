package com.example.detailsdatabase.controller;

import javafx.stage.Stage;

import java.util.Map;

public abstract class ModalController extends AbstractController {

    protected Stage stage;

    protected Map<String, Object> data;

    public void modalInitialized() {};

    protected void closeModal() {
        if(this.stage != null) {
            this.stage.close();
        }
    }

    public void setStage(Stage stage) {
        if(this.stage != null) {
            throw new IllegalArgumentException("Cannot replace stage");
        }
        this.stage = stage;
    }

    public void setData(Map<String, Object> data) {
        if(this.data != null) {
            throw new IllegalArgumentException("Cannot replace data");
        }
        this.data = data;
    }

}
