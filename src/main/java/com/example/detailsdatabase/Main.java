package com.example.detailsdatabase;

import com.example.detailsdatabase.config.Config;
import com.example.detailsdatabase.pesistence.factory.DataBaseFactory;
import com.example.detailsdatabase.window.Window;
import javafx.application.Application;
import javafx.stage.Stage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Main extends Application {

    private static final Logger LOGGER = LoggerFactory.getLogger(Main.class);

    @Override
    public void start(Stage primaryStage) throws Exception {

        Window window = Window.getInstance(primaryStage);

        try {

            Config config = Config.getInstance();

            DataBaseFactory.getInstance().getConnection();

            window.show("signIn");

        } catch (Exception e) {
            LOGGER.error("ERROR", e);
            window.error(e);
        }

    }

    public static void main(String[] args) {
        launch(args);
    }

}
