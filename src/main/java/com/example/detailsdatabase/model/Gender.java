package com.example.detailsdatabase.model;

public enum Gender {

    MALE, FEMALE;

    public static Gender get(String name) {
        try {
            return valueOf(name);
        } catch (IllegalArgumentException ignored) { }
        return null;
    }

}
