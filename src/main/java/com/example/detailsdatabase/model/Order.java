package com.example.detailsdatabase.model;

import com.example.detailsdatabase.pesistence.entity.DetailEntity;

import java.util.List;

public class Order {

    private Double price;

    private List<DetailEntity> content;

    public Order(Double price, List<DetailEntity> content) {
        this.price = price;
        this.content = content;
    }

    public Double getPrice() {
        return price;
    }

    public List<DetailEntity> getContent() {
        return content;
    }

}
