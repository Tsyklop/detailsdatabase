package com.example.detailsdatabase.config;

import com.example.detailsdatabase.util.Util;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Properties;

public class Config {

    private Properties properties;

    private static Config ourInstance;

    private static final Logger LOGGER = LoggerFactory.getLogger(Config.class);

    public static Config getInstance() {
        Config localInstance = ourInstance;
        if (localInstance == null) {
            synchronized (Config.class) {
                localInstance = ourInstance;
                if (localInstance == null) {
                    ourInstance = localInstance = new Config();
                }
            }
        }
        return localInstance;
    }

    private Config() {

        this.properties = new Properties();

        try {
            this.properties.load(Util.getResourceAsStream("application.properties"));
        } catch (IOException e) {
            LOGGER.error("CONFIG ERROR", e);
        }

    }

    public String getString(String key) {
        return this.properties.get(key).toString();
    }

    public Properties getProperties() {
        return properties;
    }

}
