package com.example.detailsdatabase.pesistence.repository.impl;

import com.example.detailsdatabase.exception.DBException;
import com.example.detailsdatabase.pesistence.entity.Entity;
import com.example.detailsdatabase.pesistence.factory.DataBaseFactory;
import com.example.detailsdatabase.pesistence.repository.Repository;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public abstract class BaseRepositoryImpl<E extends Entity> implements Repository<E> {

    protected final DataBaseFactory factory;

    protected BaseRepositoryImpl(DataBaseFactory factory) {
        this.factory = factory;
    }

    public abstract String getTableName();

    public abstract String getUpdateQuery();

    public abstract String getDeleteQuery();

    public abstract String getSelectQuery();

    public abstract String getInsertQuery();

    public List<E> getAll() throws DBException {
        String query = getSelectQuery();
        try (Connection connection = factory.getConnection();
             PreparedStatement statement = connection.prepareStatement(query);
             ResultSet resultSet = statement.executeQuery()) {
            List<E> result = new ArrayList<>();
            while (resultSet.next()) {
                result.add(parseEntity(resultSet));
            }
            return result;
        } catch (SQLException e) {
            throw new DBException(e);
        }
    }

    public Optional<E> getById(Long id) throws DBException {
        String query = getSelectQuery() + " WHERE id = ?;";
        try (Connection connection = factory.getConnection();
             PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setLong(1, id);
            try (ResultSet resultSet = statement.executeQuery()) {
                if (resultSet.next()) {
                    return Optional.of(parseEntity(resultSet));
                }
            }
        } catch (SQLException e) {
            throw new DBException(e);
        }
        return Optional.empty();
    }

    public boolean insert(E entity) throws DBException {
        String query = getInsertQuery();
        try (Connection connection = factory.getConnection();
             PreparedStatement statement = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS)) {

            prepareInsertStatement(statement, entity);

            int result = statement.executeUpdate();

            if(result > 0) {

                ResultSet rs = statement.getGeneratedKeys();

                if (rs.next()) {
                    entity.setId(rs.getLong(1));
                }

                return true;

            }

            return false;

        } catch (SQLException e) {
            throw new DBException(e);
        }
    }

    public boolean update(E entity) throws DBException {
        String query = getUpdateQuery();
        try (Connection connection = factory.getConnection();
             PreparedStatement statement = connection.prepareStatement(query)) {
            prepareUpdateStatement(statement, entity);
            int result = statement.executeUpdate();
            return result == 1;
        } catch (SQLException e) {
            throw new DBException(e);
        }
    }

    public boolean delete(E entity) throws DBException {
        String query = getDeleteQuery();
        try (Connection connection = factory.getConnection();
             PreparedStatement statement = connection.prepareStatement(query)) {
            prepareDeleteStatement(statement, entity);
            int result = statement.executeUpdate();
            return result == 1;
        } catch (SQLException e) {
            throw new DBException(e);
        }
    }

    public abstract E parseEntity(ResultSet resultSet) throws SQLException;

    public abstract void prepareInsertStatement(PreparedStatement statement, E entity);

    public abstract void prepareUpdateStatement(PreparedStatement statement, E entity);

    @Override
    public void prepareDeleteStatement(PreparedStatement statement, E entity) {
        try {
            statement.setLong(1, entity.getId());
        } catch (SQLException e) {
            throw new DBException(e);
        }
    }

}
