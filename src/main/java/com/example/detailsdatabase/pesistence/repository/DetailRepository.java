package com.example.detailsdatabase.pesistence.repository;

public interface DetailRepository {

    boolean existsByName(String name);

}
