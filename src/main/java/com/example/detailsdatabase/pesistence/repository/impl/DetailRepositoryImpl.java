package com.example.detailsdatabase.pesistence.repository.impl;

import com.example.detailsdatabase.exception.DBException;
import com.example.detailsdatabase.pesistence.entity.DetailEntity;
import com.example.detailsdatabase.pesistence.factory.DataBaseFactory;
import com.example.detailsdatabase.pesistence.repository.DetailRepository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class DetailRepositoryImpl extends BaseRepositoryImpl<DetailEntity> implements DetailRepository {

    public DetailRepositoryImpl() {
        super(DataBaseFactory.getInstance());
    }

    @Override
    public String getTableName() {
        return "detail";
    }

    @Override
    public String getUpdateQuery() {
        return "UPDATE " + getTableName() + " SET name = ?, unit = ?, article = ?, workPrice = ?, materialId = ?, materialCount = ? WHERE id = ?;";
    }

    @Override
    public String getDeleteQuery() {
        return "DELETE FROM " + getTableName() + " WHERE id = ?;";
    }

    @Override
    public String getSelectQuery() {
        return "SELECT * FROM " + getTableName() + " ";
    }

    @Override
    public String getInsertQuery() {
        return "INSERT INTO " + getTableName() + " (name, unit, article, workPrice, materialId, materialCount) VALUES(?, ?, ?, ?, ?, ?);";
    }

    @Override
    public boolean existsByName(String name) {
        String query = this.getSelectQuery() + "WHERE name = ?;";
        try (Connection connection = factory.getConnection();
             PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setString(1, name);
            try (ResultSet resultSet = statement.executeQuery()) {
                return resultSet.next();
            }
        } catch (SQLException e) {
            throw new DBException(e);
        }
    }

    @Override
    public DetailEntity parseEntity(ResultSet resultSet) throws SQLException {
        DetailEntity detailEntity = new DetailEntity();
        detailEntity.setId(resultSet.getLong("id"));
        detailEntity.setName(resultSet.getString("name"));
        detailEntity.setUnit(resultSet.getString("unit"));
        detailEntity.setArticle(resultSet.getString("article"));
        detailEntity.setWorkPrice(resultSet.getDouble("workPrice"));
        detailEntity.setMaterialId(resultSet.getLong("materialId"));
        detailEntity.setMaterialCount(resultSet.getInt("materialCount"));
        return detailEntity;
    }

    @Override
    public void prepareInsertStatement(PreparedStatement statement, DetailEntity entity) {
        try {
            fillStatement(statement, entity);
        } catch (SQLException e) {
            throw new DBException(e);
        }
    }

    @Override
    public void prepareUpdateStatement(PreparedStatement statement, DetailEntity entity) {
        try {
            fillStatement(statement, entity);
            statement.setLong(7, entity.getId());
        } catch (SQLException e) {
            throw new DBException(e);
        }
    }

    private void fillStatement(PreparedStatement statement, DetailEntity entity) throws SQLException {
        statement.setString(1, entity.getName());
        statement.setString(2, entity.getUnit());
        statement.setString(3, entity.getArticle());
        statement.setDouble(4, entity.getWorkPrice());
        statement.setLong(5, entity.getMaterialId());
        statement.setInt(6, entity.getMaterialCount());
    }

}
