package com.example.detailsdatabase.pesistence.repository.impl;

import com.example.detailsdatabase.exception.DBException;
import com.example.detailsdatabase.model.Gender;
import com.example.detailsdatabase.pesistence.entity.UserEntity;
import com.example.detailsdatabase.pesistence.factory.DataBaseFactory;
import com.example.detailsdatabase.pesistence.repository.UserRepository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Optional;

public final class UserRepositoryImpl extends BaseRepositoryImpl<UserEntity> implements UserRepository {

    public UserRepositoryImpl() {
        super(DataBaseFactory.getInstance());
    }

    @Override
    public String getTableName() {
        return "user";
    }

    @Override
    public String getUpdateQuery() {
        return "UPDATE " + getTableName() + " SET firstName = ?, lastName = ?, phone = ?, gender = ?, password = ? WHERE id = ?;";
    }

    @Override
    public String getDeleteQuery() {
        return "DELETE FROM " + getTableName() + " WHERE id = ?;";
    }

    @Override
    public String getSelectQuery() {
        return "SELECT * FROM " + getTableName() + " ";
    }

    @Override
    public String getInsertQuery() {
        return "INSERT INTO " + getTableName() + " (firstName, lastName, phone, gender, login, password) VALUES(?, ?, ?, ?, ?, ?);";
    }

    @Override
    public boolean existsByLogin(String login) {
        return findByLogin(login).isPresent();
    }

    @Override
    public Optional<UserEntity> findByLogin(String login) {
        String query = this.getSelectQuery() + "WHERE login = ?;";
        try (Connection connection = factory.getConnection();
             PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setString(1, login);
            try (ResultSet resultSet = statement.executeQuery()) {
                if (resultSet.next()) {
                    return Optional.of(this.parseEntity(resultSet));
                }
            }
        } catch (SQLException e) {
            throw new DBException(e);
        }
        return Optional.empty();
    }

    @Override
    public UserEntity parseEntity(ResultSet resultSet) throws SQLException {
        UserEntity userEntity = new UserEntity();
        userEntity.setId(resultSet.getLong("id"));
        userEntity.setFirstName(resultSet.getString("firstName"));
        userEntity.setLastName(resultSet.getString("lastName"));
        userEntity.setPhone(resultSet.getString("phone"));
        userEntity.setGender(Gender.get(resultSet.getString("gender")));
        userEntity.setLogin(resultSet.getString("login"));
        userEntity.setPassword(resultSet.getString("password"));
        return userEntity;
    }

    @Override
    public void prepareInsertStatement(PreparedStatement statement, UserEntity entity) {
        try {
            statement.setString(1, entity.getFirstName());
            statement.setString(2, entity.getLastName());
            statement.setString(3, entity.getPhone());
            statement.setString(4, entity.getGender().name());
            statement.setString(5, entity.getLogin());
            statement.setString(6, entity.getPassword());
        } catch (SQLException e) {
            throw new DBException(e);
        }
    }

    @Override
    public void prepareUpdateStatement(PreparedStatement statement, UserEntity entity) {
        try {
            statement.setString(1, entity.getFirstName());
            statement.setString(2, entity.getLastName());
            statement.setString(3, entity.getPhone());
            statement.setString(4, entity.getGender().name());
            statement.setString(5, entity.getPassword());
            statement.setLong(6, entity.getId());
        } catch (SQLException e) {
            throw new DBException(e);
        }
    }


}
