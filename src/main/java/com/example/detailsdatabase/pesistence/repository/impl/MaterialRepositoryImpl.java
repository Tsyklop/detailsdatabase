package com.example.detailsdatabase.pesistence.repository.impl;

import com.example.detailsdatabase.exception.DBException;
import com.example.detailsdatabase.pesistence.entity.MaterialEntity;
import com.example.detailsdatabase.pesistence.factory.DataBaseFactory;
import com.example.detailsdatabase.pesistence.repository.MaterialRepository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class MaterialRepositoryImpl extends BaseRepositoryImpl<MaterialEntity> implements MaterialRepository {

    public MaterialRepositoryImpl() {
        super(DataBaseFactory.getInstance());
    }

    @Override
    public String getTableName() {
        return "material";
    }

    @Override
    public String getUpdateQuery() {
        return "UPDATE " + getTableName() + " SET name = ?, unit = ?, article = ?, price = ?, shop = ?, count = ? WHERE id = ?;";
    }

    @Override
    public String getDeleteQuery() {
        return "DELETE FROM " + getTableName() + " WHERE id = ?;";
    }

    @Override
    public String getSelectQuery() {
        return "SELECT * FROM " + getTableName() + " ";
    }

    @Override
    public String getInsertQuery() {
        return "INSERT INTO " + getTableName() + " (name, unit, article, price, shop, count) VALUES(?, ?, ?, ?, ?, ?);";
    }

    @Override
    public boolean existsByName(String name) {
        String query = this.getSelectQuery() + "WHERE name = ?;";
        try (Connection connection = factory.getConnection();
             PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setString(1, name);
            try (ResultSet resultSet = statement.executeQuery()) {
                return resultSet.next();
            }
        } catch (SQLException e) {
            throw new DBException(e);
        }
    }

    @Override
    public MaterialEntity parseEntity(ResultSet resultSet) throws SQLException {
        MaterialEntity materialEntity = new MaterialEntity();
        materialEntity.setId(resultSet.getLong("id"));
        materialEntity.setName(resultSet.getString("name"));
        materialEntity.setUnit(resultSet.getString("unit"));
        materialEntity.setArticle(resultSet.getString("article"));
        materialEntity.setPrice(resultSet.getDouble("price"));
        materialEntity.setShop(resultSet.getString("shop"));
        materialEntity.setCount(resultSet.getInt("count"));
        return materialEntity;
    }

    @Override
    public void prepareInsertStatement(PreparedStatement statement, MaterialEntity entity) {
        try {
            fillStatement(statement, entity);
        } catch (SQLException e) {
            throw new DBException(e);
        }
    }

    @Override
    public void prepareUpdateStatement(PreparedStatement statement, MaterialEntity entity) {
        try {
            fillStatement(statement, entity);
            statement.setLong(7, entity.getId());
        } catch (SQLException e) {
            throw new DBException(e);
        }
    }

    private void fillStatement(PreparedStatement statement, MaterialEntity entity) throws SQLException {
        statement.setString(1, entity.getName());
        statement.setString(2, entity.getUnit());
        statement.setString(3, entity.getArticle());
        statement.setDouble(4, entity.getPrice());
        statement.setString(5, entity.getShop());
        statement.setInt(6, entity.getCount());
    }

}
