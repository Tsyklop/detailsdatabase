package com.example.detailsdatabase.pesistence.repository;

import com.example.detailsdatabase.exception.DBException;
import com.example.detailsdatabase.pesistence.entity.Entity;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

public interface Repository<E extends Entity> {

    String getTableName();

    String getUpdateQuery();

    String getDeleteQuery();

    String getSelectQuery();

    String getInsertQuery();

    List<E> getAll() throws DBException;

    boolean insert(E entity) throws DBException;
    boolean update(E entity) throws DBException;
    boolean delete(E entity) throws DBException;

    Optional<E> getById(Long id) throws DBException;

    E parseEntity(ResultSet resultSet) throws SQLException;

    void prepareInsertStatement(PreparedStatement statement, E entity) throws SQLException;
    void prepareUpdateStatement(PreparedStatement statement, E entity) throws SQLException;
    void prepareDeleteStatement(PreparedStatement statement, E entity) throws SQLException;

}
