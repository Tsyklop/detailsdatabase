package com.example.detailsdatabase.pesistence.repository;

public interface MaterialRepository {

    boolean existsByName(String name);

}
