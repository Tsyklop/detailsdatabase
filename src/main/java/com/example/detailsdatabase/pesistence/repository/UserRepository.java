package com.example.detailsdatabase.pesistence.repository;

import com.example.detailsdatabase.pesistence.entity.UserEntity;

import java.util.Optional;

public interface UserRepository {

    boolean existsByLogin(String login);

    Optional<UserEntity> findByLogin(String login);

}
