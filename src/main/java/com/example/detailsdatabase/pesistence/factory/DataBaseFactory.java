package com.example.detailsdatabase.pesistence.factory;

import com.example.detailsdatabase.config.Config;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DataBaseFactory {

    private static DataBaseFactory ourInstance;

    private static final Logger LOGGER = LoggerFactory.getLogger(DataBaseFactory.class);

    public static DataBaseFactory getInstance() {
        try {

            DataBaseFactory localInstance = ourInstance;

            if (localInstance == null) {
                synchronized (DataBaseFactory.class) {
                    localInstance = ourInstance;
                    if (localInstance == null) {
                        ourInstance = localInstance = new DataBaseFactory();
                    }
                }
            }

            return localInstance;

        } catch (ClassNotFoundException e) {
            LOGGER.error("DATABASE FACTORY ERROR", e);
            throw new RuntimeException(e);
        }

    }

    private DataBaseFactory() throws ClassNotFoundException {
        Config config = Config.getInstance();
        Class.forName(config.getString("database.class"));
    }

    public Connection getConnection() throws SQLException {

        Config config = Config.getInstance();

        StringBuilder sb = new StringBuilder();

        sb.append("jdbc:").append(config.getString("database.jdbc-type"));
        sb.append("://").append(config.getString("database.url"));
        sb.append("/").append(config.getString("database.name"));

        sb.append("?").append(config.getString("database.parameters"));

        return DriverManager.getConnection(sb.toString(), config.getString("database.login"), config.getString("database.password"));

    }


}
