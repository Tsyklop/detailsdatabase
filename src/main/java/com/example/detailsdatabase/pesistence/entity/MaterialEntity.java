package com.example.detailsdatabase.pesistence.entity;

import java.util.Objects;

public class MaterialEntity extends Entity {

    private String name;

    private String unit;

    private String article;

    private Double price;

    private String shop;

    private Integer count;

    public MaterialEntity() {}

    public MaterialEntity(String name, String unit, String article, Double price, String shop, Integer count) {
        this.name = name;
        this.unit = unit;
        this.article = article;
        this.price = price;
        this.shop = shop;
        this.count = count;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getArticle() {
        return article;
    }

    public void setArticle(String article) {
        this.article = article;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getShop() {
        return shop;
    }

    public void setShop(String shop) {
        this.shop = shop;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof MaterialEntity)) return false;
        if (!super.equals(o)) return false;
        MaterialEntity that = (MaterialEntity) o;
        return name.equals(that.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), name);
    }

    @Override
    public String toString() {
        return "MaterialEntity{" +
                "name='" + name + '\'' +
                ", unit='" + unit + '\'' +
                ", article='" + article + '\'' +
                ", price=" + price +
                ", shop='" + shop + '\'' +
                ", count=" + count +
                ", id=" + id +
                '}';
    }
}
