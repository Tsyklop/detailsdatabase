package com.example.detailsdatabase.pesistence.entity;

import java.util.Objects;

public class DetailEntity extends Entity {

    private String name;

    private String unit;

    private String article;

    private Double workPrice;

    private Long materialId;

    private Integer materialCount;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getArticle() {
        return article;
    }

    public void setArticle(String article) {
        this.article = article;
    }

    public Double getWorkPrice() {
        return workPrice;
    }

    public void setWorkPrice(Double workPrice) {
        this.workPrice = workPrice;
    }

    public Long getMaterialId() {
        return materialId;
    }

    public void setMaterialId(Long materialId) {
        this.materialId = materialId;
    }

    public Integer getMaterialCount() {
        return materialCount;
    }

    public void setMaterialCount(Integer materialCount) {
        this.materialCount = materialCount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof DetailEntity)) return false;
        if (!super.equals(o)) return false;
        DetailEntity that = (DetailEntity) o;
        return name.equals(that.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), name);
    }

    @Override
    public String toString() {
        return "DetailEntity{" +
                "name='" + name + '\'' +
                ", unit='" + unit + '\'' +
                ", article='" + article + '\'' +
                ", workPrice=" + workPrice +
                ", materialId=" + materialId +
                ", materialCount=" + materialCount +
                ", id=" + id +
                '}';
    }

}
