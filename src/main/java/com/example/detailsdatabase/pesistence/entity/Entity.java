package com.example.detailsdatabase.pesistence.entity;

import com.example.detailsdatabase.exception.UnmodifiableEntityFieldException;

import java.util.Objects;

public abstract class Entity {

    protected Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        if(this.id != null) {
            throw new UnmodifiableEntityFieldException("Cannot replace entity id");
        }
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Entity entity = (Entity) o;
        return id.equals(entity.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return "Entity{" +
                "id=" + id +
                '}';
    }

}
