package com.example.detailsdatabase.util;

import javafx.application.Platform;
import javafx.concurrent.Task;
import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.charset.Charset;

public class Util {

    public static URL getResourceURL(String path) {
        return Util.class.getClassLoader().getResource(path);
    }

    public static InputStream getResourceAsStream(String path) {
        return Util.class.getClassLoader().getResourceAsStream(path);
    }

    public static String getResourceToString(String path) throws IOException {
        return IOUtils.toString(getResourceAsStream(path), Charset.forName("UTF-8"));
    }

    public static void exit() {
        Platform.exit();
        System.exit(0);
    }

    public static void runThread(Runnable runnable) {
        Platform.runLater(runnable);
    }

    public static void runDaemonThread(Task task) {
        Thread th = new Thread(task);
        th.setDaemon(false);
        th.start();
    }

}
