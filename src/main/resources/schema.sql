CREATE TABLE IF NOT EXISTS `user`
(
    `id`        INT         NOT NULL AUTO_INCREMENT,
    `firstName` VARCHAR(45) NOT NULL,
    `lastName`  VARCHAR(45) NOT NULL,
    `phone`     VARCHAR(45) NOT NULL,
    `gender`    VARCHAR(45) NOT NULL,
    `login`     VARCHAR(45) NOT NULL,
    `password`  VARCHAR(45) NOT NULL,
    PRIMARY KEY (`id`)
)
    ENGINE = InnoDB;

CREATE UNIQUE INDEX `login_UNIQUE` ON `user` (`login` ASC) VISIBLE;


CREATE TABLE IF NOT EXISTS `material`
(
    `id`      INT         NOT NULL AUTO_INCREMENT,
    `name`    VARCHAR(45) NULL,
    `unit`    VARCHAR(45) NULL,
    `article` VARCHAR(45) NULL,
    `shop`    VARCHAR(45) NULL,
    `price`   VARCHAR(45) NULL,
    `count`   VARCHAR(45) NULL,
    PRIMARY KEY (`id`)
)
    ENGINE = InnoDB;

CREATE UNIQUE INDEX `name_UNIQUE` ON `material` (`name` ASC) VISIBLE;

CREATE TABLE IF NOT EXISTS `detail`
(
    `id`            INT         NOT NULL AUTO_INCREMENT,
    `name`          VARCHAR(45) NOT NULL,
    `unit`          VARCHAR(45) NOT NULL,
    `article`       VARCHAR(45) NOT NULL,
    `workPrice`     DOUBLE      NOT NULL DEFAULT 1,
    `materialId`    INT         NOT NULL,
    `materialCount` INT         NOT NULL DEFAULT 1,
    PRIMARY KEY (`id`, `materialId`),
    CONSTRAINT `fk_detail_material`
        FOREIGN KEY (`materialId`)
            REFERENCES `material` (`id`)
            ON DELETE NO ACTION
            ON UPDATE NO ACTION
)
    ENGINE = InnoDB;

CREATE INDEX `fk_detail_material_idx` ON `detail` (`materialId` ASC) VISIBLE;

